import '../scss/index.scss';

$('[data-toggle="tooltip"]').tooltip();
$('[data-toggle="popover"]').popover();

$('#source-modal').on('shown.bs.modal', function () {
    $('#source-modal').trigger('focus')
});