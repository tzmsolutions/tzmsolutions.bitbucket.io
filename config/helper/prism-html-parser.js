function PrismHTMLParser() {
    this.regexp = /(<code class="language-markup">)([^]+?)(<\/code>)/gm;
}

PrismHTMLParser.prototype.apply = function (compiler) {
    compiler.hooks.compilation.tap('PrismHTMLParser', (compilation) => {
        compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tapAsync(
            'PrismHTMLParser',
            (data, cb) => {
                data.html = data.html.replace(this.regexp, this.replaceBrackeys);
                cb(null, data)
            }
        )
    })
};

PrismHTMLParser.prototype.replaceBrackeys = function ($0, $1, $2, $3) {
    return $1 + $2.replace(/</g, '&lt;').replace(/>/g, '&gt;') + $3;
};

module.exports = PrismHTMLParser;