const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const path = require('path');
const PrismHTMLParser = require('./helper/prism-html-parser');

module.exports = {
    entry: {
        vendors: './src/vendors',
        library: './src/library/scripts/index.js',
        theme: './src/theme/scripts/index.js'
    },
    output: {
        path: path.resolve(process.cwd(), 'dist'),
        filename: '[name].js',
        sourceMapFilename: '[name].map'
    },
    module: {
        rules: [
            { test: /\.hbs/, loader: "handlebars-loader" }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/library/templates/index.hbs',
            filename: 'index.html'
        }),
        new WebpackCleanupPlugin('./dist', {}),
        new PrismHTMLParser()
    ]
};